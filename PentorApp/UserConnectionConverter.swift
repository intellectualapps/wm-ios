//
//  UserConnectionConverter.swift
//  PentorApp
//
//  Created by Segun Solaja on 3/23/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class UserConnectionConverter: NSObject {
    class func convert(connectionJson:Dictionary<String,AnyObject>) -> UserConnection {
        let dateFormmater = DateFormatter()
        dateFormmater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        let connection = UserConnection()
        connection.connectionId = connectionJson["connectionId"] as? String
        if let creationDate = connectionJson["creationDate"] as? String {
            connection.createdDate = dateFormmater.date(from: creationDate)
        }
        connection.currentStatus = connectionJson["currentStatus"] as? String
        if let menteeJson = connectionJson["mentee"] as? Dictionary<String,AnyObject> {
            connection.mentee = UserConverter.convert(userJson: menteeJson)
        }
        if let mentorJson = connectionJson["mentor"] as? Dictionary<String,AnyObject> {
            connection.mentor = UserConverter.convert(userJson: mentorJson)
        }
        connection.subInterestId = connectionJson["subInterestId"] as? String
        
        return connection
    }
    
}
