//
//  LauchScrennViewController.swift
//  PentorApp
//
//  Created by Tran Vuong Minh on 7/19/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class LauchScrennViewController: UIViewController {

    @IBOutlet weak var iv_gif: UIImageView!
    var timerInterval: Double = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var images: [UIImage] = []
        for i in 0...39 {
            let imageName = "frame_\(i)_delay-0.15s.gif"
            if let image = UIImage(named: imageName) {
                images.append(image)
            }
        }
        timerInterval = Double(images.count) * 0.15
        iv_gif.animationImages = images
        iv_gif.animationDuration = timerInterval
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        iv_gif.startAnimating()
        Timer.scheduledTimer(withTimeInterval: timerInterval, repeats: false) { (timer) in
           self.gotoHome()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func gotoHome() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "init")
        AppDelegate.sharedAppDelegate().window?.rootViewController = vc
    }

}
