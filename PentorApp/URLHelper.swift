//
//  URLHelper.swift
//  Truppr
//
//  Created by Segun Solaja on 10/5/15.
//  Copyright © 2015 Segun Solaja. All rights reserved.
//

import UIKit

class URLHelper: NSObject {
    
    class func getURL(_ URLKey: String) -> String {
        return self.getURL(URLKey, withURLParam: [:])
    }
    
    class func getURL(_ URLKey: String ,withURLParam: Dictionary<String,String>) -> String {
        if (!withURLParam.isEmpty){
            var str:String!
            str =  Constants.relativeURL()[URLKey] ?? ""
            for (key,value) in withURLParam{
                str = str.replacingOccurrences(of: ":" + key, with: value)
            }
            return Constants.baseURL() + str
            
        }else{
            return Constants.baseURL() + (Constants.relativeURL()[URLKey] ?? "")
        }
    }
    
}

extension Dictionary {
    func toURLEncode() -> String {
        var str: String = ""
        var arr: [String] = []
        for (key,value) in self {
            arr.append("\(key)=\(value)")
        }
        str = arr.joined(separator: "&")
        if str.isEmpty == false {
            str = "?" + str
        }
        return str
    }
}
