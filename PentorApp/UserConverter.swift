//
//  UserConverter.swift
//  PentorApp
//
//  Created by Segun Solaja on 2/12/17.
//  Copyright © 2017 Pentor. All rights reserved.
//

import UIKit

class UserConverter: NSObject {
    
    class func convert(userJson:Dictionary<String,AnyObject>?) -> User {
        guard let userJson = userJson else {
            return User()
        }
        let user  = User()
        let dateFormmater = DateFormatter()
        dateFormmater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        user.authToken = userJson["authToken"] as? String
        if let creationDate = userJson["creationDate"] as? String {
            user.createdOn = dateFormmater.date(from: creationDate)
        }
        user.email = userJson["email"] as? String
        user.firstName = userJson["firstName"] as? String
        user.lastName = userJson["lastName"] as? String
        user.isEmailVerified = userJson["isEmailAddressVerified"] as? Bool
        user.isMentor = userJson["isMentor"] as? Bool
        user.latitude = userJson["latitude"] as? Double
        user.longitude = userJson["longitude"] as? Double
        user.phoneNumber = userJson["phoneNumber"] as? String
        user.profilePhotoURL = userJson["profilePhotoUrl"] as? String
        user.longBio = userJson["longBio"] as? String
        user.gender = userJson["gender"] as? String
        user.address = userJson["address"] as? String
        user.shortBio = userJson["shortBio"] as? String
        if let birthday = userJson["birthday"] as? String {
            user.birthday = dateFormmater.date(from: birthday)
        }
        
        if let interest = userJson["mentorInterests"] as? [Dictionary<String,AnyObject>] {
            user.subInterest = interest.map({ (dict) -> SubInterest in
                self.convertSubInterest(interstJson: dict)
            })
        }
        
        return user
    }
    
    class func convertInterest(interstJson:Dictionary<String,AnyObject>) -> Interest{
        let interest  = Interest()
        interest.id = interstJson["id"] as? String
        interest.imageLink =  interstJson["imageLink"] as? String
        interest.name = interstJson["name"] as? String
        return interest
    }
    
    
    class func convertSubInterest(interstJson:Dictionary<String,AnyObject>) -> SubInterest{
        let interest  = SubInterest()
        interest.id = interstJson["id"] as? String
        interest.subId =  interstJson["subId"] as? String
        interest.name = interstJson["name"] as? String
        return interest
    }
}
